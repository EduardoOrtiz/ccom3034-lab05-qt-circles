#-------------------------------------------------
#
# Project created by QtCreator 2013-09-12T18:48:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LP
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
        Llist.cpp

HEADERS  += dialog.h \
    Llist.h
