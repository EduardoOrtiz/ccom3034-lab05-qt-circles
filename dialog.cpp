#include "dialog.h"
#include <cmath>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
    CircleX = width()/2;
    CircleY = height()/2;
}

Dialog::~Dialog()
{

}


void Dialog::mousePressEvent ( QMouseEvent * event ) {
    // output the x,y coordinates
    //qDebug() << "clicked at: " << event->x() << "," << event->y();

    QPoint P(event->x(), event->y());


    //qDebug() << P;
    //qDebug() << Member;

    if(event->button() == 1 )
    {
        Member.push(P);
        repaint();
    }

    if(event->button() == 2 && !(Member.isEmpty()))
    {
        //if(Member.isEmpty()) break;

        Node* Nodeptr = Member.first;
        int min_pos = 0;

        int X_dis = (P.x() - Nodeptr->data.x())*(P.x() - Nodeptr->data.x());
        int Y_dis = (P.y() - Nodeptr->data.y())*(P.y() - Nodeptr->data.y());

        int x_coor = Nodeptr->data.x();
        int y_coor = Nodeptr->data.y();

        double distance = sqrt(X_dis + Y_dis);

        double min_distance = distance;



        while(Nodeptr != NULL)
        {
            X_dis = (P.x() - Nodeptr->data.x())*(P.x() - Nodeptr->data.x());
            Y_dis = (P.y() - Nodeptr->data.y())*(P.y() - Nodeptr->data.y());
            distance = sqrt(X_dis + Y_dis);

            if(distance < min_distance)
            {
                min_distance = distance;
                x_coor = Nodeptr->data.x();
                y_coor = Nodeptr->data.y();
            }

            Nodeptr = Nodeptr->next;
        }

        Nodeptr = Member.first;

        while(Nodeptr != NULL)
        {
            if((Nodeptr->data.x() == x_coor) && (Nodeptr->data.y() == y_coor))
                break;

            min_pos++;

            Nodeptr = Nodeptr->next;
        }

        Member.erase(min_pos);

        repaint();
    }
}


void Dialog::paintEvent(QPaintEvent *event) {
    // the QPainter is the 'canvas' to which we will draw
    // the QPen is the pen that will be used to draw to the 'canvas'
    QPainter p(this);
    QPen myPen;

    myPen.setWidth(10);
    myPen.setColor(QColor(0xff0000));
    p.setPen(myPen);

    Node* Nodeptr = Member.first;

    while(Nodeptr != NULL)
    {
        p.drawEllipse(Nodeptr->data.x(), Nodeptr->data.y(), 20, 20);
        Nodeptr = Nodeptr->next;
    }

}

void Dialog::paint()
{
    Node* Nodeptr = Member.first;

    QPainter p(this);
    QPen myPen;

    myPen.setWidth(10);
    myPen.setColor(QColor(0xff0000));
    p.setPen(myPen);

    while(Nodeptr != NULL)
    {
        p.drawEllipse(Nodeptr->data.x(), Nodeptr->data.y(), 20, 20);
        Nodeptr = Nodeptr->next;
    }
}
