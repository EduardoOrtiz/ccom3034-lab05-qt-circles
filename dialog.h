#ifndef DIALOG_H
#define DIALOG_H

#include <QObject>
#include <QDialog>
#include <QtDebug>
#include <QtGui>
#include <QtCore>
#include "Llist.h"

class Dialog : public QDialog
{
    Q_OBJECT

private:

public:
    Dialog(QWidget *parent = 0);
    ~Dialog();
    LList Member;
    int CircleX;
    int CircleY;

protected:
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void paint();
};



#endif // DIALOG_H
