#include "Llist.h"
#include <QDebug>

void LList::insert(ElementType val, int pos) {
    
    // Validate position
    if ( (pos < 0) || (pos > mySize) ) { 
        cerr << "Attempting insert at illegal position " << pos << endl;
        exit(1);
    }
    
    // create the new node to insert
    Node *n = new Node();
    n->data.setX(val.x());
    n->data.setY(val.y());
    if (pos == 0) { 
        // inserting at the first position
        n->next = first;
        first = n;
    } else {
        Node *curr = first;
        // move to the position
        for (int i=1; i<pos; i++) curr = curr->next;  
        n->next = curr->next;
        curr->next = n;
    }
    mySize++;    
}
    
void LList::erase(int pos) {

    // Validate position
    if ( (pos < 0) || (pos > mySize - 1) ) { 
        cerr << "Attempting delete at illegal position " << pos << endl;
        exit(1);
    }
    
    Node* tmp;
    if (pos == 0) { 
        // erasing the node at the first position
        tmp = first;
        first = first->next;
    } else {
        Node *curr = first;
        // move to the position before the node to be erased
        for (int i=1; i<pos; i++)   curr = curr->next;  
        tmp = curr->next;
        curr->next = curr->next->next;
    }
    delete tmp;

    mySize--; 
}
    
void LList::display(ostream &out) const {
    Node *curr = first;
    while (curr != NULL) {
        out << curr << " ";
        curr = curr->next;
    }
    //out << "\n";
}



bool LList::operator==(const LList& L) const {
    if (mySize != L.mySize) return false; 
    Node *p = first;
    Node *q = L.first;
    while (p != NULL) {
        if (p->data != q->data) return false;
        p = p->next;
        q = q->next;
    }
    return true;
}


// Operator << overload for the nodes.
// Allows the use of qDebug() << n;  where n is a Node.

QDebug operator<< (QDebug d, const Node &n) {
    d << n.data; //<< "," << n.data.y();
    return d;
}


// Operator << overload for the List.
// Allows the use of qDebug() << L;  where L is a LList.

QDebug operator<< (QDebug d, const LList &n) {
    Node *p = n.first;
    while (p!=NULL) {
        d << *p;
        p = p->next;
    }
    return d;
}

// The classic push: inserts at the end of the list.

void LList::push(ElementType e) {
    qDebug() << "inserting " << e << " at pos: " << mySize ;
    insert(e,mySize);
}

bool LList::isEmpty()
{
    return (mySize == 0)? true : false;
}



